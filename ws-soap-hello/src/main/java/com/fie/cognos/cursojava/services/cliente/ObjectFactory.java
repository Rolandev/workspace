
package com.fie.cognos.cursojava.services.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fie.cognos.cursojava.services.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ObtenerListPersonas_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "obtenerListPersonas");
    private final static QName _Obtenerpersona_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "obtenerpersona");
    private final static QName _Saludar_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "saludar");
    private final static QName _ObtenerListPersonasResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "obtenerListPersonasResponse");
    private final static QName _SaludarResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "saludarResponse");
    private final static QName _ObtenerpersonaResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "obtenerpersonaResponse");
    private final static QName _Exception_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fie.cognos.cursojava.services.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerListPersonas }
     * 
     */
    public ObtenerListPersonas createObtenerListPersonas() {
        return new ObtenerListPersonas();
    }

    /**
     * Create an instance of {@link Obtenerpersona }
     * 
     */
    public Obtenerpersona createObtenerpersona() {
        return new Obtenerpersona();
    }

    /**
     * Create an instance of {@link Saludar }
     * 
     */
    public Saludar createSaludar() {
        return new Saludar();
    }

    /**
     * Create an instance of {@link ObtenerListPersonasResponse }
     * 
     */
    public ObtenerListPersonasResponse createObtenerListPersonasResponse() {
        return new ObtenerListPersonasResponse();
    }

    /**
     * Create an instance of {@link SaludarResponse }
     * 
     */
    public SaludarResponse createSaludarResponse() {
        return new SaludarResponse();
    }

    /**
     * Create an instance of {@link ObtenerpersonaResponse }
     * 
     */
    public ObtenerpersonaResponse createObtenerpersonaResponse() {
        return new ObtenerpersonaResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListPersonas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "obtenerListPersonas")
    public JAXBElement<ObtenerListPersonas> createObtenerListPersonas(ObtenerListPersonas value) {
        return new JAXBElement<ObtenerListPersonas>(_ObtenerListPersonas_QNAME, ObtenerListPersonas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Obtenerpersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "obtenerpersona")
    public JAXBElement<Obtenerpersona> createObtenerpersona(Obtenerpersona value) {
        return new JAXBElement<Obtenerpersona>(_Obtenerpersona_QNAME, Obtenerpersona.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Saludar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "saludar")
    public JAXBElement<Saludar> createSaludar(Saludar value) {
        return new JAXBElement<Saludar>(_Saludar_QNAME, Saludar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerListPersonasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "obtenerListPersonasResponse")
    public JAXBElement<ObtenerListPersonasResponse> createObtenerListPersonasResponse(ObtenerListPersonasResponse value) {
        return new JAXBElement<ObtenerListPersonasResponse>(_ObtenerListPersonasResponse_QNAME, ObtenerListPersonasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaludarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "saludarResponse")
    public JAXBElement<SaludarResponse> createSaludarResponse(SaludarResponse value) {
        return new JAXBElement<SaludarResponse>(_SaludarResponse_QNAME, SaludarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerpersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "obtenerpersonaResponse")
    public JAXBElement<ObtenerpersonaResponse> createObtenerpersonaResponse(ObtenerpersonaResponse value) {
        return new JAXBElement<ObtenerpersonaResponse>(_ObtenerpersonaResponse_QNAME, ObtenerpersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

}
