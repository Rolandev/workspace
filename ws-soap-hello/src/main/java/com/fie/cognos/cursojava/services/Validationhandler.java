package com.fie.cognos.cursojava.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;


public class Validationhandler implements SOAPHandler<SOAPMessageContext> {

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("--close");
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("--handleFault");
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		// TODO Auto-generated method stub
				System.out.println("-------------handleMessage--------------");
				mostrarMensajes(context);
				if(validarDatos(context)) {
					return true;	
				}
				return false;
	}

	@Override
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		System.out.println("--getHeaders");
		return null;
	}
	public boolean validarDatos(SOAPMessageContext context) {
		boolean isOutbound = (boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		// Si es salida
		if(!isOutbound) {
			System.out.println("Mensaje de entrada");
			HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
			System.out.println("IP Addr: " + request.getRemoteAddr()); 
			System.out.println("IP Host: " + request.getRemoteHost());
			System.out.println("Header[usuario]: " + request.getHeader("usuario"));
			System.out.println("Header[password]: " + request.getHeader("password"));
		}
		return true;
	}
	public void mostrarMensajes(SOAPMessageContext context){
		try {
			boolean isOutbound = (boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			// Si es salida
			if(isOutbound) {
				System.out.println("Mensaje de salida");
			} else {
				System.out.println("Mensaje de entrada");
			}
			SOAPMessage soapMessage = context.getMessage();
			soapMessage.writeTo(System.out);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}