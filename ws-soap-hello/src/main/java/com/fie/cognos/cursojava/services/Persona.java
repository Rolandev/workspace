package com.fie.cognos.cursojava.services;

public class Persona {
 private String Nombre ;
 private String ApellidoP ;
 private String ApellidoM ;
 
	Persona(String Nombre,String ApP,String ApM){
		this.Nombre= Nombre;
		this.ApellidoP= ApP;
		this.ApellidoM= ApM;
 }
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellidoP() {
		return ApellidoP;
	}
	public void setApellidoP(String apellidoP) {
		ApellidoP = apellidoP;
	}
	public String getApellidoM() {
		return ApellidoM;
	}
	public void setApellidoM(String apellidoM) {
		ApellidoM = apellidoM;
	}
 	
}
