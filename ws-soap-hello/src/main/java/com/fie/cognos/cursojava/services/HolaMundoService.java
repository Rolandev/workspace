package com.fie.cognos.cursojava.services;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
// rpc literal 
// rpc encode
// document literal
// document encode
//... entities
//		-Persona
//			nombre
//			appat
//			mat
//			edad
//Persona cargarPersona('','','','');
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@WebService
@HandlerChain(file = "handler-chain.xml")
@SOAPBinding(style = Style.DOCUMENT, use= Use.LITERAL)
public class HolaMundoService {

	@Resource
	
	WebServiceContext wsc;
	
	@WebMethod
	public String saludar() throws Exception {
		MessageContext Messa= wsc.getMessageContext();
		Map<String,Object> Header = (Map<String,Object>) Messa.get(MessageContext.HTTP_REQUEST_HEADERS);
		
		List<String> usuario = (List<String>)Header.get("usuario");
		List<String> password = (List<String>)Header.get("password");
		
		if(usuario.get(0).equals("ariel") && password.get(0).equals("123")) {
			return "iniciado";
		}
		throw new Exception("erro de coneccion");
	}
	@WebMethod
	@WebResult(name = "persona")
	public Persona obtenerpersona(@WebParam(name = "nombre") String nombre,@WebParam(name = "paterno") String paterno, @WebParam(name = "materno")String Materno) {
		return new Persona(nombre, paterno, Materno);
	}
	//generar con document literal
	@WebMethod
	public ArrayList<Persona> obtenerListPersonas() {
		//List<Persona> ListPerws = null;
		//ListPerws=ListPer;
		Persona p1= new Persona("p1", "ap1", "am1");
		Persona p2= new Persona("p2", "ap2", "am2");
		ArrayList<Persona> LisP = new  ArrayList<Persona>();
		LisP.add(p1);
		LisP.add(p2);
		return LisP;
	}
}
