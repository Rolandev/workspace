
package com.fie.cognos.cursojava.services.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para persona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="persona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apellidoM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apellidoP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "persona", propOrder = {
    "apellidoM",
    "apellidoP",
    "nombre"
})
public class Persona {

    protected String apellidoM;
    protected String apellidoP;
    protected String nombre;

    /**
     * Obtiene el valor de la propiedad apellidoM.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoM() {
        return apellidoM;
    }

    /**
     * Define el valor de la propiedad apellidoM.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoM(String value) {
        this.apellidoM = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellidoP() {
        return apellidoP;
    }

    /**
     * Define el valor de la propiedad apellidoP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellidoP(String value) {
        this.apellidoP = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

}
